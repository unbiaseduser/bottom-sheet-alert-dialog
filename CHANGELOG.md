## 2.0.2
- Add APIs that should've been in 2.0.1
  - Add versions of methods that take a layout params: `BottomSheetAlertDialogUi.setContentView`
    and `BottomSheetAlertDialogContentView.verticallyScrollable`
  - Add `BottomSheetAlertDialogContentView` factory method to create bottom padded views - since
    2.0.1 fixed bottom padding, it's only natural to add something like this.
  - Add fluent methods to `BottomSheetAlertDialogContentView` - only `verticallyScrollable` and
    `bottomPadded` are included for obvious reasons

## 2.0.1
- Fix bottom padding being added when no button is present
- Make `BottomSheetAlertDialogContentView` publicly extendable

## 2.0.0
- Project license changed from MIT to Apache 2.0
- Remove all deprecated APIs
- No longer controls the dialog's draggability
- Content view handling overhaul
  - Content frame is no longer scrollable
  - Add `setContentView` methods that take `BottomSheetAlertDialogContentView` objects to builder
    classes - its factory methods tell you everything you need to know
- Allow null parameters in builder methods
- Make `BottomSheetAlertDialogBuilder` and `BottomSheetAlertDialogFragmentViewBuilder` non-final
- Remove `initDialogBehavior`, replaced by `expandDialog` described below
- Add `expandDialog` and `collapseDialog` convenience methods for `dialog.getBehavior().setState(...)`

## 1.3.0
- Allow passing in a custom dialog in `BottomSheetAlertDialogBuilder` - this also fixes support for
  not dismissing the dialog on button click, since there was actually no way to manually dismiss the
  builder's internal dialog
- Update convenience fragment classes to match those in [base-fragments](https://gitlab.com/unbiaseduser/base-fragments)
- Deprecate `DialogButton` as the name was too generic. Replacement is `BottomSheetAlertDialogButton`.
  Add a mediator interface to help migration without duplicating library code
- Add missing `isContentViewHeightDynamic` param to `BottomSheetDialogFragment.createBottomSheetAlertDialog`

## 1.2.2
Remove `coreLibraryDesugaring` requirement - function types changed from `java.util.function.Consumer`
to `androidx.core.util.Consumer`

## 1.2.1
Fix a bug where content view can't increase height when `isContentViewHeightDynamic` is `false`
(less optimized now though), performance optimization for when `isContentViewHeightDynamic` is `true`

## 1.2.0
Add support for content views with dynamic height - the dialog's draggability changes according to
the logic introduced in version 1.1.2

## 1.1.2
- Use a single layout for both full height and non-full height content. The dialog becomes not draggable
  when the content is full-height
- Use the same layout for the button container as what `AlertDialog` uses - no more weird visual jank

## 1.1.1
Fix displaying full height content on non-full height layout

## 1.1
- Add `BottomSheetAlertDialogActions` - class that allows users to manipulate some UI behavior
- Make `BaseDialogBuilder` publicly extendable
- Extract layout dimensions
- Add `bsadTitleStyle` attribute, which gets applied as the title text's appearance
- Refine API exposure

## 1.0
Initial release